#!/bin/bash
#croncheck.sh

ip=$(ip addr show | grep inet | awk -F " " '{print $NF"|"$2}' | grep eth0 | cut -c 6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22 | cut -d/ -f1)
host=$(hostname)
date=$(date | awk {'print $2,$3,$4,$5'})

#Check to see if crond is running and, if not, start it
statuscrond=$(service crond status |awk {'print $5'})
if [ "$statuscrond" != 'running...' ];
        then
        service crond start
	echo "$host - $ip Cron service restarted on $date"  
	fi
echo  "$date - $host - $ip Cron is running" 

