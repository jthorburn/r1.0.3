# DVAUTOLOGIN script to dump key Infinsitream statistics and settings to a file

99
99
11

# Infinstream Configuration
get agent
get probe_mode
get npn_alarms
get qos_groups
get citrix thinwire_support
get mmon_options
get protocol_options
get software_options
get agent_options

# Infinstream Health and Performance
get dump free_mem
get cdmflow
get dump cdmflow
get dump tables
y
y
y
y
get skt
get app_flows info
get urt tcp_conns
get dump tcm
get dump flow_pool
get dump perf 0

# Interface Options
# if3 Configurations
set curr_interface 3
get interface_options
get dump drvstats

# if4 Configurations
set curr_interface 4
get interface_options
get dump drvstats

# if5 Configurations
set curr_interface 5
get interface_options
get dump drvstats

# if6 Configurations
set curr_interface 6
get interface_options
get dump drvstats

# if7 Configurations
set curr_interface 7
get interface_options
get dump drvstats

# if8 Configurations
set curr_interface 8
get interface_options
get dump drvstats

# if9 Configurations
set curr_interface 9
get interface_options
get dump drvstats

# if10 Configurations
set curr_interface 10
get interface_options
get dump drvstats

# if 12 Aggregate Configurations
set curr_interface 12
get interface_options
get dump drvstats

# if 13 Aggregate Configurations
set curr_interface 13
get interface_options
get dump drvstats

get eventlog
y
y
y
y
y
y
y
y
y
y
y
y
y
y
y
y
exit
