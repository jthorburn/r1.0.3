#!/bin/bash -x
#getactiveinf.sh
#
#
# VERSION 1.00
#
#
#####################################################
#
nam=$(hostname | tr '[A-Z]' '[a-z]')
day=$(date +%a_%Y%m%d-%H%M | tr '[A-Z]' '[a-z]')
#
cp /opt/scripts/ISdevicelist.csv /opt/NetScout/rtm/tools
#
cp /opt/scripts/chkinfconfig/getactiveinf.txt /opt/NetScout/rtm/tools
#
cd /opt/NetScout/rtm/tools
#
for i in `cat /opt/NetScout/rtm/tools/ISdevicelist.csv`
do
echo $i
#
./dvautologin "$i 3 DCE122r NOA140r 3 4" -i getactiveinf.txt -v >> /opt/scripts/chkinfconfig/txtfiles1/${nam}_all_active_interfaces_${day}.txt
#
done
#
mv 10.* /opt/scripts/chkinfconfig/txtfiles2
mv 167.* /opt/scripts/chkinfconfig/txtfiles2
mv 172.* /opt/scripts/chkinfconfig/txtfiles2
mv 192.* /opt/scripts/chkinfconfig/txtfiles2
rm -rf getactiveinf.txt
rm -rf ISdevicelist.csv
#
cd /opt/scripts/chkinfconfig/txtfiles2
#
for f in [1-9]*
do
echo '============================================================================' > /opt/scripts/chkinfconfig/txtfiles3/$f.tmp
grep -i "agent_name" >> /opt/scripts/chkinfconfig/txtfiles3/$f.tmp $f
grep "IP Address" >> /opt/scripts/chkinfconfig/txtfiles3/$f.tmp $f
grep -i "serial_number" >> /opt/scripts/chkinfconfig/txtfiles3/$f.tmp $f
grep -i "curr_interface" >> /opt/scripts/chkinfconfig/txtfiles3/$f.tmp $f
grep "admin_shutdown" >> /opt/scripts/chkinfconfig/txtfiles3/$f.tmp $f
grep "Link" >> /opt/scripts/chkinfconfig/txtfiles3/$f.tmp $f
echo '============================================================================' >> /opt/scripts/chkinfconfig/txtfiles3/$f.tmp
#
done
cat /opt/scripts/chkinfconfig/txtfiles3/*tmp* > /opt/scripts/chkinfconfig/txtfiles/${nam}_active_interfaces_${day}.txt
#
cd /opt/scripts/chkinfconfig/txtfiles
#
cp ${nam}_active_interfaces_${day}.txt active_interfaces.txt
#
cp active_interfaces.txt /opt/NetScout/rtm/html
#
cd /opt/scripts/chkinfconfig/txtfiles2
#
rm -rf *
#
cd /opt/scripts/chkinfconfig/txtfiles3
#
rm -rf *
#
exit
