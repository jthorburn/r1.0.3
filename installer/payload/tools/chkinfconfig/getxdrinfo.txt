# DVAUTOLOGIN script to dump Infinsitream XDR statistics to a file

99
99
99
99
11

# Infinstream Configuration
get agent

# Interface Options
# if3 Configurations
set curr_interface 3
get interface_options

# if4 Configurations
set curr_interface 4
get interface_options

# if5 Configurations
set curr_interface 5
get interface_options

# if6 Configurations
set curr_interface 6
get interface_options

# if7 Configurations
set curr_interface 7
get interface_options

# if8 Configurations
set curr_interface 8
get interface_options

# if9 Configurations
set curr_interface 9
get interface_options

# if10 Configurations
set curr_interface 10
get interface_options

# if12 Configurations
set curr_interface 12
get interface_options

# if13 Configurations
set curr_interface 13
get interface_options

#get protocol_options

#get software_options

#get mmon_options

#get track iso8583

#get dump vq

exit
