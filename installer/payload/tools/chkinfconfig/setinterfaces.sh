#!/bin/bash -x
#getinterfaces.sh
#
#
# VERSION 1.00
#
#
#####################################################
#
nam=$(hostname | tr '[A-Z]' '[a-z]')
day=$(date +%a_%Y%m%d-%H%M | tr '[A-Z]' '[a-z]')
#
cp /opt/scripts/ISdevicelist.csv /opt/NetScout/rtm/tools
#
cp /opt/scripts/chkinfinterfaces/setinterfaces.txt /opt/NetScout/rtm/tools
#
cd /opt/NetScout/rtm/tools
#
for i in `cat /opt/NetScout/rtm/tools/ISdevicelist.csv`
do
echo $i
#
./dvautologin "$i 3 DCE122r NOA140r 3 4" -i setinterfaces.txt -v >> /opt/scripts/chkinfinterfaces/txtfiles1/${nam}_all_setinterfaces_${day}.txt
#
done
#
mv 10.* /opt/scripts/chkinfinterfaces/txtfiles2
mv 167.* /opt/scripts/chkinfinterfaces/txtfiles2
mv 172.* /opt/scripts/chkinfinterfaces/txtfiles2
mv 192.* /opt/scripts/chkinfinterfaces/txtfiles2
rm -rf ISdevicelist.csv
rm -rf setinterfaces.txt
#
cd /opt/scripts/chkinfinterfaces/txtfiles2
#
for f in [1-9]*
do
echo '============================================================================' > /opt/scripts/chkinfinterfaces/txtfiles3/$f.tmp
grep -i "agent_name" >> /opt/scripts/chkinfinterfaces/txtfiles3/$f.tmp $f
grep -i "IP Address" >> /opt/scripts/chkinfinterfaces/txtfiles3/$f.tmp $f
grep -i "serial_number" >> /opt/scripts/chkinfinterfaces/txtfiles3/$f.tmp $f
grep -i "curr_interface" >> /opt/scripts/chkinfinterfaces/txtfiles3/$f.tmp $f
grep -i "rawhdr_capture" >> /opt/scripts/chkinfinterfaces/txtfiles3/$f.tmp $f
grep -i "power_alarm_util" >> /opt/scripts/chkinfinterfaces/txtfiles3/$f.tmp $f
grep -i "power_alarm_resp" >> /opt/scripts/chkinfinterfaces/txtfiles3/$f.tmp $f
echo '============================================================================' >> /opt/scripts/chkinfinterfaces/txtfiles3/$f.tmp
#
done
cat /opt/scripts/chkinfinterfaces/txtfiles3/*tmp* > /opt/scripts/chkinfinterfaces/txtfiles/${nam}_setinterfaces_${day}.txt
#
cd /opt/scripts/chkinfinterfaces/txtfiles2
#
rm -rf *
#
cd /opt/scripts/chkinfinterfaces/txtfiles3
#
rm -rf *
#
exit
