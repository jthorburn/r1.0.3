# DVAUTOLOGIN script to dump key Infinsitream statistics and settings to a file

99
99
11

# Infinstream Configuration
get agent

get npn_alarms

get qos_groups

get citrix thinwire_support

get mmon_options

get software_options

get agent_options

get protocol_options

get cdmflow

get skt

get urt tcp_conns

#xDR
get xdr 3 info
get xdr 4 info
get xdr 5 info
get xdr 6 info
get xdr 7 info
get xdr 8 info
get xdr 9 info
get xdr 10 info
get xdr 12 info
get xdr 13 info

# Interface Options
# if3 Configurations
set curr_interface 3
get interface_options

# if4 Configurations
set curr_interface 4
get interface_options

# if5 Configurations
set curr_interface 5
get interface_options

# if6 Configurations
set curr_interface 6
get interface_options

# if7 Configurations
set curr_interface 7
get interface_options

# if8 Configurations
set curr_interface 8
get interface_options

# if9 Configurations
set curr_interface 9
get interface_options

# if10 Configurations
set curr_interface 10
get interface_options

# if 12 Aggregate Configurations
set curr_interface 12
get interface_options

# if 13 Aggregate Configurations
set curr_interface 13
get interface_options

#Gn Tunneling
get gtp_opts 3
get gtp_opts 4
get gtp_opts 5
get gtp_opts 6
get gtp_opts 7
get gtp_opts 8
get gtp_opts 9
get gtp_opts 10
get gtp_opts 12
get gtp_opts 13
get tunnel table_info 3
get tunnel table_info 4
get tunnel table_info 5
get tunnel table_info 6
get tunnel table_info 7
get tunnel table_info 8
get tunnel table_info 9
get tunnel table_info 10
get tunnel table_info 12
get tunnel table_info 13

#IuPS
get sigtran 3
get sigtran 4
get sigtran 5
get sigtran 6
get sigtran 7
get sigtran 8
get sigtran 9
get sigtran 10
get sigtran 12
get sigtran 13
get iups_opts

exit
