#!/bin/bash -x
#getqoe.sh
#
#
# VERSION 1.00 Steve Murphy 10-2-2012
#
#
#####################################################
#
nam=$(hostname | tr '[A-Z]' '[a-z]')
day=$(date +%a_%Y%m%d-%H%M | tr '[A-Z]' '[a-z]')
#
cp /opt/scripts/ISdevicelist.csv /opt/NetScout/rtm/tools
#
cp /opt/scripts/chkinfconfig/getqoe.txt /opt/NetScout/rtm/tools
#
cd /opt/NetScout/rtm/tools
#
for i in `cat /opt/NetScout/rtm/tools/ISdevicelist.csv`
do
echo $i
#
./dvautologin "$i 3 DCE122r NOA140r 3 4" -i getqoe.txt -v >> /opt/scripts/chkinfconfig/txtfiles1/${nam}_getqoe_${day}.txt
#
done
#
mv 10.* /opt/scripts/chkinfconfig/txtfiles2
mv 167.* /opt/scripts/chkinfconfig/txtfiles2
mv 172.* /opt/scripts/chkinfconfig/txtfiles2
mv 192.* /opt/scripts/chkinfconfig/txtfiles2
#
cd /opt/NetScout/rtm/tools
#
rm -rf getqoe.txt
rm -rf ISdevicelist.csv
#
cd /opt/scripts/chkinfconfig/txtfiles2
#
for f in [1-9]*
do
echo '============================================================================' > /opt/scripts/chkinfconfig/txtfiles3/$f.tmp
grep -i "agent_name" >> /opt/scripts/chkinfconfig/txtfiles3/$f.tmp $f
grep -i "IP Address" >> /opt/scripts/chkinfconfig/txtfiles3/$f.tmp $f
grep -i "serial_number" >> /opt/scripts/chkinfconfig/txtfiles3/$f.tmp $f
grep -i "curr_interface" >> /opt/scripts/chkinfconfig/txtfiles3/$f.tmp $f
grep -i "vifn_mode" >> /opt/scripts/chkinfconfig/txtfiles3/$f.tmp $f
grep -i "admin_shutdown" >> /opt/scripts/chkinfconfig/txtfiles3/$f.tmp $f
echo '============================================================================' >> /opt/scripts/chkinfconfig/txtfiles3/$f.tmp
#
done
cat /opt/scripts/chkinfconfig/txtfiles3/*tmp* > /opt/scripts/chkinfconfig/txtfiles/${nam}_getqoe_${day}.txt
#
cd /opt/scripts/chkinfconfig/txtfiles2
#
rm -rf *
#
cd /opt/scripts/chkinfconfig/txtfiles3
#
rm -rf *
#
exit
