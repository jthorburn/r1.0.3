#!/bin/bash
##Check disk count and disk failures

INFOLINE=0 #line at which we start to read output
DISKCOUNTNOW=0 
DISKCOUNTEXPECT=0
DISKCOUNTOLD=0
DISKSTATE=0
DISKCONTIG=0 #disk contiguity flag
VOLSTATE=0
DISKCHPOS=0
DSKFILEIN0="/tmp/raidinfo0.stm"
DSKFILEIN1="/tmp/raidinfo1.stm"


tw_cli /c0 show > $DSKFILEIN0
tw_cli /c1 show > $DSKFILEIN1

#Lets check volume status
 
for ((i=1; i <=10; i++))
do
  if [[ `sed -n ''$i'p' $DSKFILEIN0  ` == *UnitType* ]]
  then  
        i=`expr $i + 2`
        if [[ `sed -n ''$i'p' $DSKFILEIN0 | awk ' {print $3} ' ` == "OK" ]]
	then
          VOLSTATE=0
	  break
	else
 	  VOLSTATE=1
          break
	fi	
  fi
done

if [[ $VOLSTATE -eq 0 ]]
then
  for ((i=1; i <=10; i++))
  do
    if [[ `sed -n ''$i'p' $DSKFILEIN1  ` == *UnitType* ]]
    then
      i=`expr $i + 2`
      if [[ `sed -n ''$i'p' $DSKFILEIN1 | awk ' {print $3} ' ` == "OK" ]]
      then
        VOLSTATE=0
        else
	 VOLSTATE=1
      fi
  fi
done
fi

echo VOLSTATE=$VOLSTATE

  
#Check all of the disks are present and accounted for  - CONTROLLER 0
INFOLINE=1
DISKCOUNTEXPECT=0
for ((a=1; a <= 30 ; a++)) 
do
DISKCOUNTNOW=`sed -n ''$INFOLINE'p' $DSKFILEIN0 | cut -d"p" -f2 | awk ' {print $1} ' `
if [[ $DISKCOUNTNOW == [0-9] ]]
  then
  if [[ $DISKCOUNTNOW != $DISKCOUNTEXPECT ]]
  then
    DISKCONTIG=1 
  fi	
  DISKCOUNTEXPECT=`expr $DISKCOUNTEXPECT + 1`
fi
INFOLINE=`expr $INFOLINE + 1`
done 


#Check all of the disks are present and accounted for  - CONTROLLER 1
INFOLINE=1
DISKCOUNTEXPECT=0                                                                                                 
for ((a=1; a <= 30 ; a++))                                                                                        
do                                                                                                                
DISKCOUNTNOW=`sed -n ''$INFOLINE'p' $DSKFILEIN1 | cut -d"p" -f2 | awk ' {print $1} ' `                            
if [[ $DISKCOUNTNOW == [0-9] ]]                                                                                   
  then                                                                                                            
  if [[ $DISKCOUNTNOW != $DISKCOUNTEXPECT ]]                                                                      
  then                                                                                                            
    DISKCONTIG=1                                                                                                  
  fi                                                                                                              
  DISKCOUNTEXPECT=`expr $DISKCOUNTEXPECT + 1`                                                                     
fi                                                                                                                
INFOLINE=`expr $INFOLINE + 1`                                                                                     
done   


echo DISKCONT=$DISKCONTIG

#Set the pointer to check for disk status
INFOLINE=1
while [ 1 ]
do                                                                                                                
read myLine || break 
if [[ `sed -n ''$INFOLINE'p' $DSKFILEIN0 | cut -d"p" -f2 | awk ' {print $1} ' ` == Port ]]
then
  INFOLINE=`expr $INFOLINE + 2`
  break
else
  INFOLINE=`expr $INFOLINE + 1`
fi
done < $DSKFILEIN0


#Now lets check if any of the disks have failed
 
while [ 1 ]
do                                                                                                                
read myLine || break
STATECACHE=`sed -n ''$INFOLINE'p' $DSKFILEIN0 | cut -d"p" -f2 | awk ' {print $2} ' `                            
DELIMSTAT=`sed -n ''$INFOLINE'p' $DSKFILEIN0 | awk ' {print $1} ' | cut -b1 `
if [[ $DELIMSTAT != p ]]
then
  break
fi
if [[ $STATECACHE == OK ]]                                                                                   
then                                                                                                            
  DISKSTATE=0                                                                                                  
else
  DISKSTATE=1   
  break
fi                                                                                                                
INFOLINE=`expr $INFOLINE + 1`                                                                                     
done < $DSKFILEIN0     


if [[ $DISKSTATE == 0 ]]
then
  #Set the pointer to check for disk status
  INFOLINE=1
  while [ 1 ]
  do
  read myLine || break
  if [[ `sed -n ''$INFOLINE'p' $DSKFILEIN1 | cut -d"p" -f2 | awk ' {print $1} ' ` == Port ]]
  then
    INFOLINE=`expr $INFOLINE + 2`
    break
  else
    INFOLINE=`expr $INFOLINE + 1`
  fi
  done < $DSKFILEIN1



 #Now lets check if any of the disks have failed
 while [ 1 ]
 do
 read myLine || break
 STATECACHE=`sed -n ''$INFOLINE'p' $DSKFILEIN1 | cut -d"p" -f2 | awk ' {print $2} ' `
 DELIMSTAT=`sed -n ''$INFOLINE'p' $DSKFILEIN1 | awk ' {print $1} ' | cut -b1 `
 if [[ $DELIMSTAT != p ]]
 then
   break
 fi
 if [[ $STATECACHE == OK ]]
 then
   DISKSTATE=0
 else
   DISKSTATE=1
   break
 fi
 INFOLINE=`expr $INFOLINE + 1`
 done < $DSKFILEIN1

fi


echo DISKSTATE=$DISKSTATE

if [[ DISKSTATE -ne 0 || VOLSTATE -ne 0 || DISKCONTIG -ne 0 ]]
then
  exit 1
else
  exit 0
fi
