#!/bin/bash
PATH=/usr/kerberos/sbin:/usr/kerberos/bin:/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/opt/dell/srvadmin/bin:/opt/dell/srvadmin/sbin:/root/bin

cat /opt/nshealth/live_head.txt > /opt/nshealth/livestats.html

today=$(date +%Y%m%d)

cd /opt/nshealth

FILES=islive/*.txt
  for f in $FILES
  do

ipaddr=$(echo $f | cut -d/ -f2 | cut -d_ -f1)

if grep -q  FAIL $f  &&  grep -q GOOD $f
then
    echo "<a href=\"$f\" target=\"_blank\" class=\"btn btn-live\" btn-danger-text style=\"margin-left:auto;\"" >> /opt/nshealth/livestats.html
    echo "</i>$ipaddr</a>" >> /opt/nshealth/livestats.html
elif grep -q FAIL $f
then
   echo "<a href=\"$f\" target=\"_blank\" class=\"btn btn-danger\" btn-mini style=\"margin-left:auto;\""  >> /opt/nshealth/livestats.html
   echo "</i>$ipaddr</a>" >> /opt/nshealth/livestats.html
elif grep -q GOOD $f
then
  echo "<a href=\"$f\" target=\"_blank\" class=\"btn btn-success\" btn-mini style=\"margin-left:auto;\""  >> /opt/nshealth/livestats.html
  echo "</i>$ipaddr</a>" >> /opt/nshealth/livestats.html
else
   echo "<a href=\"$f\" target=\"_blank\" class=\"btn btn-primary\" btn-mini style=\"margin-left:auto;\""  >> /opt/nshealth/livestats.html
  echo "</i>$ipaddr</a>" >> /opt/nshealth/livestats.html
fi
  done

cat live_middle.txt >> /opt/nshealth/livestats.html

cat /opt/nshealth/live_bottom.txt >> /opt/nshealth/livestats.html
