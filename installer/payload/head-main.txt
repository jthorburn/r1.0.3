<!DOCTYPE html PUBLIC "XHTML 1.0 Transitional//EN" >
<html xmlns="Netscout">
<head>
<title>Netscout Systems - Health Check</title>
<meta http-equiv=refresh content=900>
<meta name="description" content="Health Check">
<meta name="keywords" content="[Health Check">
<meta name="author" content="Netscout Systems">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/coin-slider.css" />
<link rel="stylesheet" type="text/css" href="css/epoch_styles.css" /> <!--Epoch's styles-->
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/epoch_classes.js"></script> <!--Epoch's Code-->
<script type="text/javascript" src="js/cufon-yui.js"></script>
<script type="text/javascript" src="js/cufon-aller-700.js"></script>
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript" src="js/coin-slider.min.js"></script>
<script type="text/javascript">


var calendar1, calendar2, calendar3; /*must be declared in global scope*/
/*put the calendar initializations in the window's onload() method*/
window.onload = function() {
	calendar1 = new Epoch('cal1','flat',document.getElementById('calendar1_container'),false);
	calendar2 = new Epoch('cal2','popup',document.getElementById('calendar2_container'),false);
	calendar3 = new Epoch('cal3','flat',document.getElementById('calendar3_container'),true);
};
</script>
<script>
<!--
function land(ref, target)
{
lowtarget=target.toLowerCase();
if (lowtarget=="_self") {window.location=loc;}
else {if (lowtarget=="_top") {top.location=loc;}
else {if (lowtarget=="_blank") {window.open(loc);}
else {if (lowtarget=="_parent") {parent.location=loc;}
else {parent.frames[target].location=loc;};
}}}
}
function jump(menu, frm)
{
var n, part1, part2, dateformat; //AAAAMM

if (frm.date.value == ""){
	Date.prototype.toUSAdate = function(delim)
{ // Change Date object to formatted USA Date string
  delim = (delim) ? delim.toString().substr(0,1) : "/";
  return ((this.getMonth()+1).toPaddedString(2)) + delim +
         (this.getDate().toPaddedString(2)) + delim +
         (this.getFullYear().toPaddedString(4));
}
Number.prototype.toPaddedString = function(len, pad)
{ // Change number or string to a padded string
	len = (len) ? Number(len) : 2;
	if (isNaN(len)) {
		alert("Padded String 'length' argument is not numeric.");
		return null;
	}
	var dflt = (isNaN(this.toString())) ? " " : "0";
	pad = (pad) ? pad.toString().substr(0,1) : dflt;
	var str = this.toString();
	if (dflt == "0") {
		while (str.length < len) str = pad + str;
	} else {
		while (str.length < len) str += pad;
	}
	return str;
}
String.prototype.toPaddedString = Number.prototype.toPaddedString;
	}
	else{
	dateformat = frm.date.value.substr(6,4)+frm.date.value.substr(0,2)+frm.date.value.substr(3,2);
	ref=menu.choice.options[menu.choice.selectedIndex].value;
		
	n=ref.search("AAAAMM");
	if(n!=-1){
		part1=ref.substr(0,n);
		part2=ref.substr(n+6,ref.length);
		ref=part1 + dateformat + part2;
		//alert(ref);
	}
		
	splitc=ref.lastIndexOf("*");
	target="";
	if (splitc!=-1){
		loc=ref.substring(0,splitc);
		target=ref.substring(splitc+1,1000);	
	}else{
		loc=ref; target="_self";
	};
	if (ref != "") {land(loc,target);}
};
}
//-->
</script>
</head>
<body>
<div class="main">
  <div class="header">
    <div class="header_resize">
        
    
