#!/bin/bash

cat /opt/nshealth/head-main.txt > /opt/nshealth/index.html

   ipaddress=$(ip addr show | grep inet | awk -F " " '{print $NF"|"$2}' | grep eth0 | cut -c 6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22 | cut -d/ -f1)
   hostname=$(hostname)
   echo "   <div class="logo">" >> /opt/nshealth/index.html
   echo "   <h1> <small>$hostname</small><small><span>$ipaddress</span></small> </a></h1>" >> /opt/nshealth/index.html
   echo "   </div>" >> /opt/nshealth/index.html

cat /opt/nshealth/head-end.txt >>  /opt/nshealth/index.html


today=$(date +%Y%m%d)
FILES=ishealth/*_$today.txt
  for f in $FILES
  do
 hostname=$(echo $f | cut -d/ -f2 | cut -d_ -f1) 
 filename=$(echo $f | cut -d_ -f1)
echo "<option value="$filename\_AAAAMM.txt"> $hostname </option>"  >> /opt/nshealth/index.html
done

cat /opt/nshealth/middle1.txt >> /opt/nshealth/index.html

today=$(date +%Y%m%d)
FILES=pmhealth/*_$today.txt
  for i in $FILES
  do
 hostname=$(echo $i | cut -d/ -f2 | cut -d_ -f1)
 filename=$(echo $i | cut -d_ -f1)
echo "<option value="$filename\_AAAAMM.txt"> $hostname </option>"  >> /opt/nshealth/index.html
done


cat /opt/nshealth/middle2.txt >> /opt/nshealth/index.html

today=$(date +%Y%m%d)
FILES=ishealth/scrub*_$today.txt
  for i in $FILES
  do
 hostname=$(echo $i | cut -d/ -f2 | cut -d_ -f1)
 filename=$(echo $i | cut -d_ -f1)
echo "<option value="$filename\_AAAAMM.txt"> $hostname </option>"  >> /opt/nshealth/index.html
done


cat /opt/nshealth/middle3.txt >> /opt/nshealth/index.html

today=$(date +%Y%m%d)
FILES=pmhealth/scrub*_$today.txt
  for i in $FILES
  do
 hostname=$(echo $i | cut -d/ -f2 | cut -d_ -f1)
 filename=$(echo $i | cut -d_ -f1)
echo "<option value="$filename\_AAAAMM.txt"> $hostname </option>"  >> /opt/nshealth/index.html
done


cat /opt/nshealth/middle4.txt >> /opt/nshealth/index.html




FILES=ishealth/*_$today.txt
  for f in $FILES
  do


if egrep -iw "fail|Error|Bad|degraded" $f | egrep -v "No|Count: 0|Number: 0|Chassis Intrusion|Unconfigured|exist"
then
  echo "<a href=\"$f\" class=\"tab red\">" >> /opt/nshealth/index.html
  echo "<span class=\"left\"></span>" >> /opt/nshealth/index.html
  echo "<span class=\"right\"></span>" >> /opt/nshealth/index.html
  echo "</a>" >> /opt/nshealth/index.html
elif egrep -iw "Warning" $f | egrep -v "No|Count: 0|Number: 0|Chassis Intrusion|Unconfigured|exist"
then
  echo "<a href=\"$f\" class=\"tab orange\">" >> /opt/nshealth/index.html
  echo "<span class=\"left\"></span>" >> /opt/nshealth/index.html
  echo "<span class=\"right\"></span>" >> /opt/nshealth/index.html
  echo "</a>" >> /opt/nshealth/index.html
elif [ -s $f ] 
then
  echo "<a href=\"$f\" class=\"tab green\">" >> /opt/nshealth/index.html
  echo "<span class=\"left\"></span>" >> /opt/nshealth/index.html
  echo "<span class=\"right\"></span>" >> /opt/nshealth/index.html
  echo "</a>" >> /opt/nshealth/index.html
else
  echo "<a href=\"$f\" class=\"tab blue\">" >> /opt/nshealth/index.html
  echo "<span class=\"left\"></span>" >> /opt/nshealth/index.html
  echo "<span class=\"right\"></span>" >> /opt/nshealth/index.html
  echo "</a>" >> /opt/nshealth/index.html
fi
  done

cat /opt/nshealth/middle5.txt >> /opt/nshealth/index.html

FILES=pmhealth/*_$today.txt
  for f in $FILES
  do


if egrep -i "fail|Warning|Error|Bad|degraded" $f | egrep -i -v "No|display|predictive|Media|other|ok|good"
then
  echo "<a href=\"$f\" class=\"tab red\">" >> /opt/nshealth/index.html
  echo "<span class=\"left\"></span>" >> /opt/nshealth/index.html
  echo "<span class=\"right\"></span>" >> /opt/nshealth/index.html
  echo "</a>" >> /opt/nshealth/index.html
else
  echo "<a href=\"$f\" class=\"tab green\">" >> /opt/nshealth/index.html
  echo "<span class=\"left\"></span>" >> /opt/nshealth/index.html
  echo "<span class=\"right\"></span>" >> /opt/nshealth/index.html
  echo "</a>" >> /opt/nshealth/index.html

fi
  done

cat /opt/nshealth/bottom.txt >> /opt/nshealth/index.html
