#!/bin/bash


date
echo
echo *******Check Infinistreams*******
echo
for i in `cat /opt/nshealth/infinip.csv`;
do
/usr/bin/ssh root@$i ip addr show | grep inet | awk -F " " '{print $NF"|"$2}' | grep eth0 | cut -c 6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22 | cut -d/ -f1
echo
/usr/bin/ssh root@$i ls ishealth/
echo '++++++++++++++++++++++++++'
done
