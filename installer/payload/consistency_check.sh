#!/bin/bash -xv
#Monthly consistency check script for Dell HW

#Get the path of omreport
omreport1=`which omreport`
echo $omreport1

#If omreport is not in the path, see if omreport is on the system
if ($omreport1="");
then
	if [ -a /usr/bin/omreport ];
	then
		omreport1=`/usr/bin/omreport`
	
		if [ -a /opt/dell/srvadmin/bin/omreport ];
		then
			omreport1=`/opt/dell/srvadmin/bin/omreport`
		fi

	fi
#echo "omreport is not installed on this device or isn't in the usual places"
fi

#Get the controller and vdisk info using omreport
controller=`$omreport1 storage controller |grep ^ID |sed -n 's/.*\(.\)$/\1/p'`
vdisk=`$omreport1 storage vdisk |grep ^ID |sed -n 's/.*\(.\)$/\1/p'`

#Get count of controllers and vdisks
count_cntrl=`$omreport1 storage controller |grep ^ID |sed -n 's/.*\(.\)$/\1/p' |wc -l`
count_vdisks=`$omreport1 storage vdisk |grep ^ID |sed -n 's/.*\(.\)$/\1/p' |wc -l`

echo "Number of Controllers = "$count_cntrl
echo "Number of VDISKS = "$count_vdisks
echo
#Verify that omconfig is on the system
#Get the path of omconfig
omconfig1=`which omconfig`
echo $omconfig1


#Start the Consistemcy check
for (( i = 0; i < $count_cntrl; i++ ))
	do
		for (( j = 0; j < $count_vdisks; j++ ))
		do
			$omconfig1 storage vdisk action=checkconsistency controller=$i vdisk=$j
		done

		echo "Should be done"
	done
exit
