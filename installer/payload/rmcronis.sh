#!/bin/bash

crontab -l >CRON_TEMP
awk '$0!~/ishealth/ { print $0 }' CRON_TEMP >CRON_NEW
crontab CRON_NEW
