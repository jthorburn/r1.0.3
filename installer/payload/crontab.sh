#!/bin/bash

dialog --title "Crontab Install" \
--backtitle "Health Check Installer v1.0" \
--infobox "  Added Dohealh in crontab for daily run" 4 50 ; sleep 2

crontab -l > date.cron

if ! grep -Fq "/opt/nshealth/dohealth" date.cron; then
echo '1 3 * * * ./root/.bashrc; /opt/nshealth/dohealth >> /opt/nshealth/logs/infinistreamhealth.log 2>&1' >> date.cron
fi

if ! grep -Fq "/opt/nshealth/consistency_check" date.cron; then
echo '0 1 1 * * ./root/.bashrc; /opt/nshealth/consistency_check.sh >> /opt/nshealth/logs/consistency.log 2>&1' >> date.cron
fi

if ! grep -Fq "/opt/nshealth/getinfini-configfile.sh" date.cron; then
echo '0 1 2 * * ./root/.bashrc; /opt/nshealth/getinfini-configfile.sh >> /opt/nshealth/logs/infini-configfile.log 2>&1' >> date.cron
fi

if ! grep -Fq "/opt/nshealth/do-dvgetconfig.sh" date.cron; then
echo '0 1 3 * * ./root/.bashrc; /opt/nshealth/do-dvgetconfig.sh >> /opt/nshealth/logs/dvgentconfig.log 2>&1' >> date.cron
fi

if ! grep -Fq "/opt/nshealth/generate.sh" date.cron; then 
echo '50 6 * * * ./root/.bashrc; /opt/nshealth/generate.sh >> /opt/nshealth/logs/generate.log 2>&1' >> date.cron
fi

if ! grep -Fq "/opt/nshealth/generate.sh" date.cron; then
echo '50 7 * * * ./root/.bashrc; /opt/nshealth/generate.sh >> /opt/nshealth/logs/generate.log 2>&1' >> date.cron
fi

if ! grep -Fq "/opt/nshealth/grabisoutput" date.cron; then
echo '31 5 * * * ./root/.bashrc; /opt/nshealth/grabisoutput >> /opt/nshealth/logs/grabisoutput.log 2>&1' >> date.cron
fi

if ! grep -Fq "/opt/nshealth/intupdate.sh" date.cron; then
echo '*/16 * * * * ./root/.bashrc; /opt/nshealth/intupdate.sh >> /opt/nshealth/logs/intupdate.log 2>&1' >> date.cron
fi

if ! grep -Fq "/opt/nshealth/get_is_status_files.sh" date.cron; then
echo '*/15 * * * * . /root/.bashrc; /opt/nshealth/get_is_status_files.sh >> /tmp/is_status.log 2>&1' >> date.cron
fi

if ! grep -Fq "/opt/nshealth/get_pm_status_files.sh" date.cron; then
echo '*/17 * * * * . /root/.bashrc; /opt/nshealth/get_pm_status_files.sh >> /tmp/pm_status.log 2>&1' >> date.cron
fi

if ! grep -Fq "/opt/nshealth/generate_live.sh" date.cron; then
echo '*/22 * * * * . /root/.bashrc; /opt/nshealth/generate_live.sh >> /tmp/genlive.log 2>&1' >> date.cron
fi

if ! grep -Fq "/opt/nshealth/generatenews.sh" date.cron; then
echo '*/25 * * * * . /root/.bashrc; /opt/nshealth/generatenews.sh >> /opt/nshealth/logs/generatenews.log 2>&1' >> date.cron
fi

if ! grep -Fq "/opt/nshealth/scruboutput" date.cron; then
echo '30 6 * * * . /root/.bashrc; /opt/nshealth/scruboutput >> /opt/nshealth/logs/generatenews.log 2>&1' >> date.cron
fi

crontab date.cron
rm -f date.cron

dialog --title "***Job added to Crontab***" \
--backtitle "Health Check Installer v1.0" \
--infobox "$(crontab -l)" 30 120 ; sleep 3

