#!/bin/bash

#PATH=/usr/kerberos/sbin:/usr/kerberos/bin:/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/opt/dell/srvadmin/bin:/opt/dell/srvadmin/sbin:/root/bin

cd /opt/nshealth

cat /opt/nshealth/head-main.txt > /opt/nshealth/index.html

   ip=`/sbin/ip addr show | grep inet | awk -F " " '{print $NF"|"$2}' | grep eth0 | cut -c 6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22 | cut -d/ -f1`
   hostname=$(hostname)
   echo "   <div class="logo">" >> /opt/nshealth/index.html
   echo "   <h1> <small>$hostname</small><small><span>$ip</span></small> </a></h1>" >> /opt/nshealth/index.html
   echo "   </div>" >> /opt/nshealth/index.html

cat /opt/nshealth/head-end.txt >>  /opt/nshealth/index.html


today=$(date +%Y%m%d)
FILES=ishealth/*_$today.txt
  for f in $FILES
  do
 hostname=$(echo $f | cut -d/ -f2 | cut -d_ -f1) 
 filename=$(echo $f | cut -d_ -f1)
echo "<option value="$filename\_AAAAMM.txt"> $hostname </option>"  >> /opt/nshealth/index.html
done

cat /opt/nshealth/middle1.txt >> /opt/nshealth/index.html

today=$(date +%Y%m%d)
FILES=pmhealth/*_$today.txt
  for i in $FILES
  do
 hostname=$(echo $i | cut -d/ -f2 | cut -d_ -f1)
 filename=$(echo $i | cut -d_ -f1)
echo "<option value="$filename\_pmhealth_AAAAMM.txt"> $hostname </option>"  >> /opt/nshealth/index.html
done


cat /opt/nshealth/middle2.txt >> /opt/nshealth/index.html

today=$(date +%Y%m%d)
FILES=ishealth/scrub*_$today.scrub
  for i in $FILES
  do
 hostname=$(echo $i | cut -d/ -f2 | cut -d_ -f1)
 filename=$(echo $i | cut -d_ -f1)
echo "<option value="$filename\_AAAAMM.scrub"> $hostname </option>"  >> /opt/nshealth/index.html
done


cat /opt/nshealth/middle3.txt >> /opt/nshealth/index.html

today=$(date +%Y%m%d)
FILES=pmhealth/scrub*_$today.scrub
  for i in $FILES
  do
 hostname=$(echo $i | cut -d/ -f2 | cut -d_ -f1)
 filename=$(echo $i | cut -d_ -f1)
echo "<option value="$filename\_AAAAMM.scrub"> $hostname </option>"  >> /opt/nshealth/index.html
done


cat /opt/nshealth/middle4.txt >> /opt/nshealth/index.html




FILES=$(ls ishealth/*_$today.txt | grep -v summary)
  for f in $FILES
  do

if grep "IP:" $f
then
ipaddr=$(grep "IP:" $f | awk '{print $2}')
else
ipaddr="No IP Addr"
fi
echo "THE IP = $ipaddr"

if egrep -iw "fail|Error|Bad|degraded" $f | egrep -v "No|Count: 0|Number: 0|Chassis Intrusion|Unconfigured|exist"
then
echo "<a href=\"$f\" target=\"_blank\" class=\"btn btn-danger\" btn-default style=\"margin-left:auto;\"" >> /opt/nshealth/index.html
  echo "</i>$ipaddr</a>" >> /opt/nshealth/index.html

elif egrep -iw "Warning" $f | egrep -v "No|Count: 0|Number: 0|Chassis Intrusion|Unconfigured|exist"
then
  echo "<a href=\"$f\" target=\"_blank\" class=\"btn btn-warning\" btn-default style=\"margin-left:auto;\"" >> /opt/nshealth/index.html
  echo "</i>$ipaddr</a>" >> /opt/nshealth/index.html

elif egrep -i "such|permission denied|line 7" $f |  egrep -v "No such file or directory|Connection closed"
then
  echo "<a href=\"$f\" target=\"_blank\" class=\"btn btn-primary\" btn-default style=\"margin-left:auto;\"" >> /opt/nshealth/index.html
  echo "</i>$ipaddr</a>" >> /opt/nshealth/index.html

else
  echo "<a href=\"$f\" target=\"_blank\" class=\"btn btn-success\" btn-default style=\"margin-left:auto;\"" >> /opt/nshealth/index.html
  echo "</i>$ipaddr</a>" >> /opt/nshealth/index.html
fi
  done



#had to add this fix the blue icon duplication on drop down menu

ipaddr=unknown

FILES=ishealth/*_$today.status
  for f in $FILES
  do

  ipaddr=$(echo $f | cut -d/ -f2 | cut -d_ -f1)

if egrep -iw "No such|permission denied" $f
then

  echo "<a href=\"$f\" target=\"_blank\" class=\"btn btn-primary\" btn-default style=\"margin-left:auto;\"" >> /opt/nshealth/index.html
  echo "</i>$ipaddr</a>" >> /opt/nshealth/index.html

fi
  done





cat /opt/nshealth/middle5.txt >> /opt/nshealth/index.html

FILES=pmhealth/*_$today.txt
  for f in $FILES
  do

if grep "IP: " $f
then
ipaddr=$(grep "IP: " $f | awk '{print $2}')
else
ipaddr="No IP Addr"
fi

if egrep -i "fail$|failed$|Error$|Bad|degraded" $f | egrep -i -v "No|display|predictive|Media|other|ok|good|verification failed"
 then
   echo "<a href=\"$f\" target=\"_blank\" class=\"btn btn-danger\" btn-default style=\"margin-left:auto;\"" >> /opt/nshealth/index.html
   echo "</i>$ipaddr</a>" >> /opt/nshealth/index.html
elif egrep -iw "Warning" $f | egrep -v "No|Count: 0|Number: 0|Chassis Intrusion|Unconfigured|exist|WARNING: REMOTE HOST"
then
  echo "<a href=\"$f\" target=\"_blank\" class=\"btn btn-warning\" btn-default style=\"margin-left:auto;\"" >> /opt/nshealth/index.html
  echo "</i>$ipaddr</a>" >> /opt/nshealth/index.html

elif egrep -iw "No such|permission denied|Host key verification failed|Connection timed out|Connection closed by remote host|Connection closed|port 22|verification failed" $f 
then
echo "<a href=\"$f\" target=\"_blank\" class=\"btn btn-primary\" btn-default style=\"margin-left:auto;\"" >> /opt/nshealth/index.html
 echo "</i>$ipaddr</a>" >> /opt/nshealth/index.html
else
   echo "<a href=\"$f\" target=\"_blank\" class=\"btn btn-success\" btn-default style=\"margin-left:auto;\"" >> /opt/nshealth/index.html
   echo "</i>$ipaddr</a>" >> /opt/nshealth/index.html
fi
  done

cat /opt/nshealth/bottom.txt >> /opt/nshealth/index.html
