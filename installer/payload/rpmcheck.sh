#!/bin/bash

rpm=$(rpm -qa | grep ^dialog)

if [ "$rpm" -eq "" ] > 2&>1
then
echo "Installing RPM to run installer"
rpm -ivh dialog-1.1-9.20080819.1.el6.x86_64.rpm
else
echo "RPM file already exists, starting installer"
fi

