#!/bin/sh 
#scruboutput
echo "" > ISscrub.scrub

today=$(date +%Y%m%d)
cd /opt/nshealth/ishealth
FILES=*_$today.txt
  for f in $FILES
 do
cd /opt/nshealth/ishealth
./scrub.sh $f
done

find /  -name "scrub*.txt" -exec rename .txt .scrub {} \;

FILES=*_$today.scrub
for f in $FILES
do
mv $f >> ISscrub.scrub
