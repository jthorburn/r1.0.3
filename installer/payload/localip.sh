#!/bin/bash


ip addr show | grep inet | awk -F " " '{print $NF"|"$2}' | grep eth0 | cut -c 6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22 | cut -d/ -f1 > ip.txt
