#!/bin/bash

crontab -l > date.cron

if ! grep -Fq "IScheck.sh" date.cron
then
touch date.cron
echo '1 12 * * * ./root/.bashrc;/root/ishealth/IScheck.sh  > infinistreamhealth.log 2>&1' >> date.cron
fi
crontab date.cron
rm -f date.cron
