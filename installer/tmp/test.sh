#!/bin/bash
#Install script

dialog --title "Health Check Install" \
--backtitle "Health Check Installer v1.0" \
--infobox "Installing Healthchecks files" 4 40 ; sleep 3

dialog --title "Health Check Install" \
--backtitle "Health Check Installer v1.0" \
--infobox "Creating directories" 4 30 ; sleep 3

#Verify if the /opt/nshealth directory exists. If not, create it
if [ ! -d "/opt/nshealth" ]; then
mkdir /opt/nshealth &
fi


if [ ! -d "/opt/nshealth/logs" ]; then
mkdir /opt/nshealth/logs/ &
fi

#if [ ! -d "/opt/nshealth/pmhealth" ]; then
mkdir /opt/nshealth/pmhealth/ &
#fi

#if [ ! -d "/opt/nshealth/ishealth" ]; then
mkdir /opt/nshealth/ishealth/ &
#fi

if [ ! -d "/opt/backup"; then
mkdir /opt/backup &
fi

if [ ! -d "/opt/backup/infinistream-configfiles/"; then
mkdir /opt/backup/infinistream-configfiles/
fi

dialog --title "Health Check Install" \
--backtitle "Health Check Installer v1.0" \
--infobox "Creating backup directory for infinistreams config backups" 4 100 ; sleep 4

dialog --title "Backup Directory Created" \
--backtitle "Health Check Installer v1.0" \
--infobox "$(find /opt/backup  -type f -maxdepth 1)" 15 45 ; sleep 4



dialog --title "Health Check Install" \
--backtitle "Health Check Installer v1.0" \
--infobox "Copying files to 'nshealth' directory" 4 50 ; sleep 3


#Copy healthcheck files over to /opt/nshealth

mv diskchk /opt/nshealth/ &
mv dohealth /opt/nshealth/ &
mv healthck /opt/nshealth/ & 
mv infinick /opt/nshealth/ &
mv infinip.csv /opt/nshealth/ &
mv infinssh /opt/nshealth/ &
mv mem.sh /opt/nshealth/ &
mv globhealthck /opt/nshealth/ &
mv scruboutput /opt/nshealth/ &
mv infinihealthchk /opt/nshealth/ &
mv KPS /opt/nshealth/ &
mv consistency_check.sh /opt/nshealth/ &
mv do-dvgetconfig.sh /opt/nshealth/ &
mv getinfini-configfile.sh /opt/nshealth/ &

# Change permissions on healthcheck script files

chmod +x /opt/nshealth/* &

# Make directories to store the Infinistream and PM healthcheck results


#Copy Scrub files over to /opt/nshealth/ishealth and /pmhealth 

cp scrub.sh /opt/nshealth/pmhealth/ &
cp scrub.sh /opt/nshealth/ishealth/ & 
cp scrublist.csv /opt/nshealth/pmhealth/ & 
cp scrublist.csv /opt/nshealth/ishealth/ &

# Change permissions on scrub script files

chmod +x /opt/nshealth/pmhealth/* &
chmod +x /opt/nshealth/ishealth/* &

rm -f /opt/scrub.sh &
rm -f /opt/scrublist.csv &

dialog --title "Directories Created" \
--backtitle "Health Check Installer v1.0" \
--infobox "$(find /opt/nshealth  -type f -maxdepth 1)" 20 45 ; sleep 4

dialog --title "Health Check Install" \
--backtitle "Health Check Installer v1.0" \
--infobox "Adding dohealth script in Crontab for daily run" 4 50 ; sleep 3

crontab -l > date.cron
echo '1 3 * * * ./root/.bashrc; /opt/nshealth/dohealth >> /opt/nshealth/logs/infinistreamhealth.log 2>&1' >> date.cron
echo '0 1 1 * * ./root/.bashrc; /opt/nshealth/consistency_check.sh >> /opt/nshealth/logs/consistency.log 2>&1' >> date.cron
echo '0 1 2 * * ./root/.bashrc; /opt/nshealth/getinfini-configfile.sh >> /opt/nshealth/logs/infini-configfile.log 2>&1' >> date.cron
echo '0 1 3 * * ./root/.bashrc; /opt/nshealth/do-dvgetconfig.sh >> /opt/nshealth/logs/dvgentconfig.log 2>&1' >> date.cron
crontab date.cron
rm -f date.cron

dialog --title "Here is your latest Crontab" \
--backtitle "Health Check Installer v1.0" \
--infobox "$(crontab -l)" 10 150 ; sleep 4

dialog --title "Health Check Install" \
--backtitle "Health Check Installer v1.0" \
--infobox "Copying all ACTIVE Infinistream IPs to infinip.csv"  4 60 ; sleep 3

day=$(date +%a | tr '[A-Z]' '[a-z]')
cat /opt/NetScout/rtm/log/flowloggerdebuglog-${day}.txt | grep "Inf" | grep "ACT" | cut -c 1-15 > /opt/scripts/infinip.csv

dialog --title "List of all ISs IPs in infinip.csv file" \
--backtitle "Health Check Installer v1.0" \
--infobox "$(cat /opt/nshealth/infinip.csv)" 50 60 ; sleep 3 

dialog --title "Health Check Install" \
--backtitle "Health Check Installer v1.0" \
--infobox "Creating and Copying RSA key to ISs. You will be prompted to enter Infinistreams passwords" 4 100 ; sleep 3

dialog --title "Health Check Install" \
--backtitle "Health Check Installer v1.0" \
--infobox "Exiting HealthCheck Installer to create the RSA keys" 4 70 ; sleep 3

clear
cd /opt/nshealth/
./infinssh

echo "**************************************************"
echo "** Full Health check install has been completed **"
echo "**************************************************"


for i in `cat /opt/nshealth/infinip.csv`;
do
/usr/bin/ssh root@$i /root/iscron.sh >> /opt/nshealth/crontab.txt
echo
done


