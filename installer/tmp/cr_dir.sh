#!/bin/bash
#cr_dir

if [ ! -d "/opt/nshealth" ]; then
mkdir /opt/nshealth
dialog --title "Creating Healthcheck directories" \
--backtitle "Health Check Installer v1.0" \
--infobox "   /opt/nshealth/ created" 4 40 ; sleep 2
else
dialog --title "Creating Healthcheck directories" \
--backtitle "Health Check Installer v1.0" \
--infobox " ***** DIRECTORY /opt/nshealth ALREADY EXISTS *****" 4 60 ; sleep 3
fi

if [ ! -d "/opt/nshealth/logs" ]; then
mkdir /opt/nshealth/logs
dialog --title "Creating Healthcheck directories" \
--backtitle "Health Check Installer v1.0" \
--infobox "  /opt/nshealth/logs created" 4 50 ; sleep 2
else
dialog --title "Creating Healthcheck directories" \
--backtitle "Health Check Installer v1.0" \
--infobox "***** DIRECTORY /opt/nshealth/logs ALREADY EXIST *****" 4 60 ; sleep 3
fi

if [ ! -d "/opt/nshealth/pmhealth" ]; then
mkdir /opt/nshealth/pmhealth
dialog --title "Creating Healthcheck directories" \
--backtitle "Health Check Installer v1.0" \
--infobox "/opt/nshealth/pmhealth created" 4 50 ; sleep 2
else
dialog --title "Creating Healthcheck directories" \
--backtitle "Health Check Installer v1.0" \
--infobox "***** DIRECTORY /opt/nshealth/pmhealth ALREADY EXIST *****" 4 65 ; sleep 3
fi

if [ ! -d "/opt/nshealth/ishealth" ]; then
mkdir /opt/nshealth/ishealth
dialog --title "Creating Healthcheck directories" \
--backtitle "Health Check Installer v1.0" \
--infobox "/opt/nshealth/ishealth created" 4 50 ; sleep 2
else
dialog --title "Creating Healthcheck directories" \
--backtitle "Health Check Installer v1.0" \
--infobox "***** DIRECTORY /opt/nshealth/ishealth ALREADY EXIST *****" 4 70 ; sleep 3
fi

dialog --title "Creating Healthcheck directories" \
--backtitle "Health Check Installer v1.0" \
--infobox "         ===== ALL HEALTH CHECK DIRECTORIES HAS BEEN CREATED =====" 4 80 ; sleep 2

dialog --title "Directories Created" \
--backtitle "Health Check Installer v1.0" \
--infobox "$(du /opt/nshealth/ | cut -f 2)" 6 40 ; sleep 3

