#!/bin/bash

dialog --title "Crontab Install" \
--backtitle "Health Check Installer v1.0" \
--infobox "  Added Dohealh in crontab for daily run" 4 50 ; sleep 2

crontab -l > date.cron

if ! grep -Fq "dohealth" date.cron 
then
touch date.cron
echo '1 3 * * * ./root/.bashrc; /opt/nshealth/dohealth >> /opt/nshealth/logs/infinistreamhealth.log 2>&1' >> date.cron
fi
if ! grep -Fq "consistency_check" date.cron
then
echo '0 1 1 * * ./root/.bashrc; /opt/nshealth/consistency_check.sh >> /opt/nshealth/logs/consistency.log 2>&1' >> date.cron
fi
if ! grep -Fq "getinfini-configfile.sh" date.cron
then
echo '0 1 2 * * ./root/.bashrc; /opt/nshealth/getinfini-configfile.sh >> /opt/nshealth/logs/infini-configfile.log 2>&1' >> date.cron
fi
if ! grep -Fq "do-dvgetconfig.sh" date.cron
then
echo '0 1 3 * * ./root/.bashrc; /opt/nshealth/do-dvgetconfig.sh >> /opt/nshealth/logs/dvgentconfig.log 2>&1' >> date.cron
fi
crontab date.cron
rm -f date.cron

dialog --title "***Job added to Crontab***" \
--backtitle "Health Check Installer v1.0" \
--infobox "$(crontab -l)" 20 100 ; sleep 3

