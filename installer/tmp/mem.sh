#!/bin/bash

for i in `ls $1*`
do
echo $i | awk '{print $13, $1}' $i | sort -rn | head -5
done
