#!/bin/bash

cat /opt/nshealth/live_head.txt > /opt/nshealth/livestats.html

today=$(date +%Y%m%d)

cd /opt/nshealth

FILES=islive/*.txt
  for f in $FILES
  do

if grep -q  FAIL $f  &&  grep -q GOOD $f
then
  echo "<a href=\"$f\" class=\"tab Fgreen\">" >> /opt/nshealth/livestats.html
  echo "<span class=\"left\"></span>" >> /opt/nshealth/livestats.html
  echo "<span class=\"right\"></span>" >> /opt/nshealth/livestats.html
  echo "</a>" >> /opt/nshealth/livestats.html
elif grep -q FAIL $f
then
  echo "<a href=\"$f\" class=\"tab red\">" >> /opt/nshealth/livestats.html
  echo "<span class=\"left\"></span>" >> /opt/nshealth/livestats.html
  echo "<span class=\"right\"></span>" >> /opt/nshealth/livestats.html
  echo "</a>" >> /opt/nshealth/livestats.html
elif grep -q GOOD $f
then
  echo "<a href=\"$f\" class=\"tab green\">" >> /opt/nshealth/livestats.html
  echo "<span class=\"left\"></span>" >> /opt/nshealth/livestats.html
  echo "<span class=\"right\"></span>" >> /opt/nshealth/livestats.html
  echo "</a>" >> /opt/nshealth/livestats.html
else
echo "<a href=\"$f\" class=\"tab blue\">" >> /opt/nshealth/livestats.html
  echo "<span class=\"left\"></span>" >> /opt/nshealth/livestats.html
  echo "<span class=\"right\"></span>" >> /opt/nshealth/livestats.html
  echo "</a>" >> /opt/nshealth/livestats.html
fi
  done

cat live_middle.txt >> /opt/nshealth/livestats.html

#FILES=pmhealth/*$today.txt
FILES=pmlive/*.txt
  for f in $FILES
  do


if grep -q Complete $f
  then

#if egrep -i "fail|Warning|Error|Bad|degraded" $f | egrep -i -v "No|display|predictive|Media|other|ok|good"

if egrep  -i "fail|Warn|Error|Bad|degraded|missing|not|100%|99%" $f 
then
  echo "<a href=\"$f\" class=\"tab red\">" >> /opt/nshealth/livestats.html
  echo "<span class=\"left\"></span>" >> /opt/nshealth/livestats.html
  echo "<span class=\"right\"></span>" >> /opt/nshealth/livestats.html
  echo "</a>" >> /opt/nshealth/livestats.html
elif
grep Utilization $f | cut -d= -f4 | cut -d"," -f1 | egrep "16|95|96|97|98" 
then
  echo "<a href=\"$f\" class=\"tab orange\">" >> /opt/nshealth/livestats.html
  echo "<span class=\"left\"></span>" >> /opt/nshealth/livestats.html
  echo "<span class=\"right\"></span>" >> /opt/nshealth/livestats.html
  echo "</a>" >> /opt/nshealth/livestats.html
else
  echo "<a href=\"$f\" class=\"tab green\">" >> /opt/nshealth/livestats.html
  echo "<span class=\"left\"></span>" >> /opt/nshealth/livestats.html
  echo "<span class=\"right\"></span>" >> /opt/nshealth/livestats.html
  echo "</a>" >> /opt/nshealth/livestats.html
fi
else
echo "<a href=\"$f\" class=\"tab blue\">" >> /opt/nshealth/livestats.html
  echo "<span class=\"left\"></span>" >> /opt/nshealth/livestats.html
  echo "<span class=\"right\"></span>" >> /opt/nshealth/livestats.html
  echo "</a>" >> /opt/nshealth/livestats.html
fi
  done

cat /opt/nshealth/live_bottom.txt >> /opt/nshealth/livestats.html
