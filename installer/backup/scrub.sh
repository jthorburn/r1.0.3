#!/bin/sh
#scrub.sh
#Version 2.0

for i in `/bin/cat /opt/scripts/infinip.csv`;
do
/usr/bin/ssh root@$i /root/whoami
done

# Check for root user

[ "$(whoami)" != 'root' ] && ( echo you are using a non-privileged account; exit 0 ) 

# Create data file - omit if you have your own external file.

host=$(hostname | tr '[a-z]' '[A-Z]')
var2=Health

echo $host
echo $1

# Set IFS (internal field separator)
# This determines how Bash recognizes fields when it looks at strings.
# We are setting a var to the original IFS value so that we may assign a "," as our new value for the loop.

IFS=$(echo -en "\n\b")
OIFS=$IFS
#x=$(grep -i $lookingFor $1 | wc -l)

while IFS="," read lookingFor numLines ommitData
	do

#~~~~~~~~~~~~~~~~~~~~~~~
    x=$(grep -i $lookingFor $1 | wc -l)
#    echo " -------------> Found $x instances of $lookingFor" | grep -v " 0 $lookingFor" | awk '{printf "%-38s%-20s%+5s%-1s%-10s\n", $1, $2, $3, "", $4}' >> scrubd-$1;
#~~~~~~~~~~~~~~~~~~~~~~~

# The above loop reads in the file from "done<{filename.csv}"

  echo ' ' >> scrubd-$1;
  echo '______________________________________________________'  >> scrubd-$1;
  echo '||                                                  ||'  >> scrubd-$1;
  echo '        Found ['$x'] Instances of "'$lookingFor'"     '  >> scrubd-$1;
  echo '||__________________________________________________||'  >> scrubd-$1;
  echo ' ' >> scrubd-$1;


#echo ' ' >> scrubd-$1;


###----------
  if [ -n ommitData ] 
     then 
	echo '      'Not looking for '*'$ommitData'*' from $lookingFor 
   echo '      'Ommited '*'$ommitData'*' from $lookingFor search >> scrubd-$1;
	echo >> scrubd-$1;


#	echo $ommitData
	  if grep -iq $lookingFor $1   # This performs a case insentive grep quitely to check to see if data exists


	    then

		grep -in -A $numLines $lookingFor $1 | egrep -vi '('$ommitData')' >> scrubd-$1 ;    # This is the work horse to search the healthcheck

	    else
	        echo '!!!! '$lookingFor' does not appear in the file !!!!' >> scrubd-$1;
	  fi

     else 

###----------

          if grep -iq $lookingFor $1                                  # This performs a case insentive grep quitely to check to see if data exists
            then
                grep -in -A $numLines $lookingFor $1 >> scrubd-$1 ;    # This is the work horse to search the healthcheck
            else
                echo '!!!! '$lookingFor' does not appear in the file !!!!' >> scrubd-$1;
          fi
  fi
done <scrublist.csv
IFS=$OIFS

exit 0

