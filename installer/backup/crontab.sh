#!/bin/bash

dialog --title "Crontab Install" \
--backtitle "Health Check Installer v1.0" \
--infobox "Added Dohealh in crontab for daily run" 4 60 ; sleep 2

crontab -l > date.cron
touch date.cron
echo '1 3 * * * /opt/scripts/dohealth >> /opt/scripts/logs/infinistreamhealth.log 2>&1' >> date.cron
crontab date.cron
rm -f date.cron

dialog --title "Crontab Install" \
--backtitle "Health Check Installer v1.0" \
--infobox "Job added in Crontab" 4 40 ; sleep 2


dialog --title "Crontab Install" \
--backtitle "Health Check Installer v1.0" \
--yesno "Would you like to check your crontab now?" 7 60

response=$?
case $response in
   0) echo 'crontab -l';;
   1) echo break;;
   255) echo "[ESC] key pressed.";;
esac
