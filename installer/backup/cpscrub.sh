#!/bin/bash
#cpscrub.sh

#Copy Scrub files over to /opt/scripts/infini-healthck and /opt/scripts/pm-healthchk

dialog --title "Scrub Files Install" \
--backtitle "Health Check Installer v1.0" \
--infobox "Installing scrub files" 4 40 ; sleep 2


if [ ! -d "/opt/scripts/infini-healthck" ]; then
echo
dialog --title "Scrub Files Install" \
--backtitle "Health Check Installer v1.0" \
--infobox "There is no infini-healthck directory" 4 40 ; sleep 2
echo
echo

else

cp scrub.sh /opt/scripts/infini-healthck
cp scrublist.csv /opt/scripts/infini-healthck

# Change permissions on scrub script files

chmod +x /opt/scripts/infini-healthck/*

rm -f /opt/scrub.sh
rm -f /opt/scrublist.csv

dialog --title "Scrub Files Install" \
--backtitle "Health Check Installer v1.0" \
--infobox "scrub files has been copied to infini-healthck directory" 4 40 ; sleep 2

dialog --title "Scrub Files Install" \
--backtitle "Health Check Installer v1.0" \
--yesno "Would you like to see the files in infini-healthck directory? \"/tmp/scrub.txt\"?" 7 60

response=$?
case $response in
   0) echo 'ls -lh /opt/scripts/infini-healthck/ | grep scrub*';;
   1) break;;
   255) echo "[ESC] key pressed.";;
esac
fi
