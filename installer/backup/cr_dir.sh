#!/bin/bash
#cr_dir

if [ ! -d "/opt/scripts" ]; then
mkdir /opt/scripts
fi
dialog --title "Creating Healthcheck directories" \
--backtitle "Health Check Installer v1.0" \
--infobox "   /opt/scripts/ created" 4 40 ; sleep 2

if [ ! -d "/opt/scripts/logs" ]; then
mkdir /opt/scripts/logs
dialog --title "Creating Healthcheck directories" \
--backtitle "Health Check Installer v1.0" \
--infobox "  /opt/scripts/logs created" 4 50 ; sleep 2
fi

if [ ! -d "/opt/scripts/pm-healhchk" ]; then
mkdir /opt/scripts/pm-healthchk
dialog --title "Creating Healthcheck directories" \
--backtitle "Health Check Installer v1.0" \
--infobox "/opt/scripts/pm-healthchk created" 4 50 ; sleep 2
fi

if [ ! -d "/opt/scripts/infini-healthck" ]; then
mkdir /opt/scripts/infini-healthck
dialog --title "Creating Healthcheck directories" \
--backtitle "Health Check Installer v1.0" \
--infobox "/opt/scripts/infini-healthck created" 4 50 ; sleep 2
fi

dialog --title "Creating Healthcheck directories" \
--backtitle "Health Check Installer v1.0" \
--infobox "         ===== ALL HEALTH CHECK DIRECTORIES HAS BEEN CREATED =====" 4 80 ; sleep 2

dialog --title "Directories Created" \
--backtitle "Health Check Installer v1.0" \
--infobox "$(du /opt/scripts/)" 20 40 ; sleep 3

