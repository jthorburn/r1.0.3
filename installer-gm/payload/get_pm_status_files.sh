#!/bin/bash

for i in `cat /opt/nshealth/pmip.csv` 
  do
   /usr/bin/scp -o ConnectTimeout=5 root@$i:/tmp/status.txt /opt/nshealth/pmlive/$i"_status.txt" 2> /opt/nshealth/pmlive/$i"_status.txt"  
   /usr/bin/ssh  -o ConnectTimeout=5 root@$i "mv -f /tmp/status.txt /tmp/status.old" 
done

/opt/nshealth/generate-gm.sh
