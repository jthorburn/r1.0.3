
#!/bin/bash

cat /opt/nshealth/head-menu.txt > /opt/nshealth/pm-menu.html

port=$(cat webport.txt)
today=$(date +%Y%m%d)


#if it is port 8080 that was entered. it will use this loop
if [ $port -eq 8080 ]
then

for i in `cat /opt/nshealth/pmip.csv`;
  do

echo "<div class="example">" >> /opt/nshealth/pm-menu.html
echo "<a TARGET="content" href="http://$i:$port/nshealth" class="button"> $i </a>"  >> /opt/nshealth/pm-menu.html
echo "</div>" >> /opt/nshealth/pm-menu.html
done

cat /opt/nshealth/bottom-menu.txt >> /opt/nshealth/pm-menu.html

#if it is port 80 that was entered. it will use this loop
elif [ $port -eq 80 ]
then

for i in `cat /opt/nshealth/pmip.csv`;
  do

echo "<div class="example">" >> /opt/nshealth/pm-menu.html
echo "<a TARGET="content" href="http://$i:$port/nshealth" class="button"> $i </a>"  >> /opt/nshealth/pm-menu.html
echo "</div>" >> /opt/nshealth/pm-menu.html
done

cat /opt/nshealth/bottom-menu.txt >> /opt/nshealth/pm-menu.html

else

#if it is port 8443,443 or any other than 8080 and 80 that was entered. it will use this loop

for i in `cat /opt/nshealth/pmip.csv`;
  do

echo "<div class="example">" >> /opt/nshealth/pm-menu.html
echo "<a TARGET="content" href="https://$i:$port/nshealth" class="button"> $i </a>"  >> /opt/nshealth/pm-menu.html
echo "</div>" >> /opt/nshealth/pm-menu.html
done

cat /opt/nshealth/bottom-menu.txt >> /opt/nshealth/pm-menu.html

fi
