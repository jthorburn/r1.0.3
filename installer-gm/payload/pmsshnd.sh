#!/bin/bash
#pmssh



host=$(hostname | tr '[A-Z]' '[a-z]')
echo ""
echo "***********************************************"
echo "CREATING KEYS - CONFIRMATION AND LOGIN REQUIRED"
echo "***********************************************"
sleep 2

if [ ! -f "/root/.ssh/id_rsa.pub" ]; then
ssh-keygen -t rsa

else
for i in `cat /opt/nshealth/pmipnd.csv`;
do
/usr/bin/scp /root/.ssh/id_rsa.pub $i:/root/${host}_rsa.pub
/usr/bin/ssh root@$i "


if [ ! -d .ssh ]; then mkdir .ssh ; chmod 700 .ssh ; fi
cd .ssh/
if [ ! -f authorized_keys2 ]; then touch authorized_keys ; chmod 600 authorized_keys ; fi 
cat /root/${host}_rsa.pub >>authorized_keys2
cat /root/${host}_rsa.pub >>authorized_keys
"
done
fi

echo ""
echo "*******************************"
echo "RSA Keys has been copied to PMs"
echo "*******************************"
sleep 2
