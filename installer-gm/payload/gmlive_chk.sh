#!/bin/bash

PATH=/usr/kerberos/sbin:/usr/kerberos/bin:/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/opt/dell/srvadmin/bin:/opt/dell/srvadmin/sbin:/root/bin

ip=$(ip addr show | grep inet | awk -F " " '{print $NF"|"$2}' | grep eth0 | cut -c 6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22 | cut -d/ -f1)

#Check if omreport is running; if not, it will start it
/etc/init.d/dataeng status | if egrep -qiv "running"
then
        modprobe mptctl
        /etc/init.d/dataeng restart
        sleep 5
echo ****omreport started****
fi


om_path=`which omreport`
hostname > /tmp/status.txt
echo "" >> /tmp/status.txt
date >> /tmp/status.txt
echo "" >> /tmp/status.txt
rdate=$(date +%a | tr [:upper:] [:lower:])
$om_path chassis >> /tmp/status.txt
echo "" >> /tmp/status.txt
echo "Drive status:" >> /tmp/status.txt
echo "" >> /tmp/status.txt
$om_path storage pdisk controller=0 | grep Status | egrep -v "Power|Life" >> /tmp/status.txt
echo "" >> /tmp/status.txt
echo "Memory Usage:" >> /tmp/status.txt
echo "" >> /tmp/status.txt
tail -n5 /opt/NetScout/rtm/log/*memory*$rdate*  >> /tmp/status.txt
echo ""
echo "Check Complete" >> /tmp/status.txt

cp /tmp/status.txt /opt/nshealth/pmlive/${ip}_status.txt
