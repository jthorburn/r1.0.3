#!/bin/bash


date
echo
echo *******Cron Check PMs*******
echo
#Check to see if crond is running and, if not, start it
for i in `cat /opt/nshealth/pmip.csv`;
do
/usr/bin/ssh -o ConnectTimeout=10 root@$i 'bash -s' < /opt/nshealth/croncheck-pm.sh >> /opt/nshealth/logs/pmcron.log

done
/opt/nshealth/croncheck-pm.sh >> /opt/nshealth/logs/pmcron.log
echo
exit
echo Cron Check Completed
exit
