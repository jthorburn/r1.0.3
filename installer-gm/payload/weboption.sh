#!/bin/bash

1=""
2=""
3=""
4=""
 
# open fd
exec 3>&1
 
# Store data to $VALUES variable
VALUES=$(dialog --ok-label "Submit" \
	  --backtitle "HealthCheck installer v1.0" \
	  --title "Please type the port you use for web access: " \
	  --form "Enter a port number" \
10 50 0 \
	"Port #:"     1 1 "$1" 	1 5 5 0 \
2>&1 1>&3)
 
# close fd
exec 3>&-
 
# display values just entered
echo "$VALUES" > /opt/nshealth/webport.txt

