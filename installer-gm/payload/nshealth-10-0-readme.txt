NS Health 1.0 Build 0 Information

Version:  1.0

Build Number:  0


Release Date:  3/7/2013
	    
Platforms:  Linux


Installation Instructions:
-------------------------- 

PM LINUX:
     Log in as root
     Make sure to make a copy of your current crontab
     vi /opt/NetScout/apache/conf/httpd.conf file and add the following attribute:
	Alias /nshealth "/opt/nshealth/"

     	You should be adding this attribute under the Alias session as shown below.

	Alias /media "/opt/NetScout/rtm/media"
	<Directory "/opt/NetScout/rtm/media">
   	Options FollowSymLinks
	</Directory>
	Alias /icons/ "/opt/NetScout/apache/icons/"

	<Directory "/opt/NetScout/apache/icons">
    	Options Indexes MultiViews
    	AllowOverride None
    	Order allow,deny
    	Allow from all
	</Directory>
	Alias /nshealth "/opt/nshealth/"

     Make sure this Alias is added to all PMs and GM if applicable
     Make sure you restart the web services (webstop/webstart) for every PM/GM

     Execute <location of patch>/nshealth-r1.0.00.bin 
   	Menu Options:
		1) it will install the complete health check on the PM and its infinistreams.	  
		2) If you need to modify a file or install healthcheck to a new device.
		3) This PSS tools comes with the full install, however you can install them by itself if you dont want to install the health checks.
		4) Full uninstall of health check.
		5) exit installer  


     Execute /opt/nshealth/generate.sh
   
Please allow 24 hours to build data and update the GUI data accordingly.

	To take you to the main screen of the healthcheck, type IP address of the PM/GM X.X.X.X:8080/nshealth

------------------------------------------------------------------------------
Where to download this version: 


Premium Support Services > Premium Support Services Shared Documents > NSHealth > nshealth-r1.0.0.bin




For More Information check x.x.x.x:8080/nshealth/PSSGuide.html
