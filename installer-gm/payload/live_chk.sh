#!/bin/bash

PATH=/usr/kerberos/sbin:/usr/kerberos/bin:/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/opt/dell/srvadmin/bin:/opt/dell/srvadmin/sbin:/root/bin


om_path=`which omreport`
hostname > /tmp/status.txt
echo "" >> /tmp/status.txt
date >> /tmp/status.txt
echo "" >> /tmp/status.txt
rdate=$(date +%a | tr [:upper:] [:lower:])
$om_path chassis >> /tmp/status.txt
echo "" >> /tmp/status.txt
echo "Drive status:" >> /tmp/status.txt
echo "" >> /tmp/status.txt
$om_path storage pdisk controller=0 | grep Status | egrep -v "Power|Life" >> /tmp/status.txt
echo "" >> /tmp/status.txt
echo "Memory Usage:" >> /tmp/status.txt
echo "" >> /tmp/status.txt
tail -n5 /opt/NetScout/rtm/log/*memory*$rdate*  >> /tmp/status.txt
echo ""
echo "Check Complete" >> /tmp/status.txt

