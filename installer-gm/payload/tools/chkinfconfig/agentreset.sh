#!/bin/bash -x
#agentreset.sh
#
#
# VERSION 1.00
#
#
#####################################################
#
nam=$(hostname | tr '[A-Z]' '[a-z]')
day=$(date +%a_%Y%m%d-%H%M | tr '[A-Z]' '[a-z]')
#
cp /opt/scripts/ISdevicelist.csv /opt/NetScout/rtm/tools
#
cp /opt/scripts/chkinfinterfaces/agentreset.txt /opt/NetScout/rtm/tools
#
cd /opt/NetScout/rtm/tools
#
for i in `cat /opt/NetScout/rtm/tools/ISdevicelist.csv`
do
echo $i
#
./dvautologin "$i 3 DCE122r NOA140r 3 4" -i agentreset.txt
#
done
#
mv 10.* /opt/scripts/chkinfinterfaces/txtfiles2
mv 167.* /opt/scripts/chkinfinterfaces/txtfiles2
mv 172.* /opt/scripts/chkinfinterfaces/txtfiles2
mv 192.* /opt/scripts/chkinfinterfaces/txtfiles2
rm -rf agent.txt
rm -rf ISdevicelist.csv
#
cd /opt/scripts/chkinfinterfaces/txtfiles2
#
rm -rf *
#
exit
