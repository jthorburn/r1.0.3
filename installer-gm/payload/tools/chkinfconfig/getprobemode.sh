#!/bin/bash -x
#getprobemode.sh
#
#
# VERSION 1.00
#
#
#####################################################
# Ready script for ksh or Bash
ECHO=echo
[[ $(basename $SHELL) == bash ]] && ECHO="echo -e"
####################################################
#
############### DEFINE FUNCTIONS ###################
#
elapsed_time ()
{
SEC=$1
(( SEC < 60 )) && $ECHO "[Elapsed time: $SEC seconds]\n"
(( SEC >= 60 && SEC < 3600 )) && $ECHO "[Elapsed time: $(( SEC / 60 )) min $(( SEC % 60 )) sec]\n"
(( SEC > 3600 )) && $ECHO "[Elapsed time: $(( SEC / 3600 )) hr $((  (SEC % 3600) / 60 )) min $(( (SEC % 3600) % 60 ))sec]\n"
}
#
####################################################
#
nam=$(hostname | tr '[A-Z]' '[a-z]')
day=$(date +%a_%Y%m%d-%H%M | tr '[A-Z]' '[a-z]')
#
SECONDS=0
START_TIME=`date +%s`
echo "Started at:" `date` > /opt/scripts/chkinfconfig/txtfiles/${nam}_grep_getprobemode_${day}.txt
echo
echo Get Citrix Thinwire Support Setting
echo
cp /opt/scripts/ISdevicelist.csv /opt/NetScout/rtm/tools
#
cp /opt/scripts/chkinfconfig/getprobemode.txt /opt/NetScout/rtm/tools
#
cd /opt/NetScout/rtm/tools
#
for i in `cat /opt/NetScout/rtm/tools/ISdevicelist.csv`
do
echo $i
#
./dvautologin "$i 3 DCE122r NOA140r 3 4" -i getprobemode.txt -v >> /opt/scripts/chkinfconfig/txtfiles/${nam}_getprobemode_${day}.tmp1
#
done
#
cd /opt/scripts/chkinfconfig/txtfiles
#
cat -n ${nam}_getprobemode_${day}.tmp1 > ${nam}_getprobemode_${day}.txt
#
cat ${nam}_getprobemode_${day}.txt | grep -i "agent_name" >> ${nam}_grep_getprobemode_${day}.txt
cat ${nam}_getprobemode_${day}.txt | grep -i "Probe Mode" >> ${nam}_grep_getprobemode_${day}.txt
rm -rf ${nam}_getprobemode_${day}.tmp*
#
elapsed_time $SECONDS  >> /opt/scripts/chkinfconfig/txtfiles/${nam}_grep_getprobemode_${day}.txt
$ECHO
FINISH_TIME=`date +%s`
echo "Finished at:" `date` >> /opt/scripts/chkinfconfig/txtfiles/${nam}_grep_getprobemode_${day}.txt
exit
