#!/bin/bash
#statusmon.sh
#
mv /tmp/statusmon.log /tmp/statusmon.log.old
sleep 3
mv /tmp/statusmon.eml /tmp/statusmon.eml.old
sleep 3
mv /tmp/recalert.log /tmp/recalert.old
sleep 3
MAILMODE=1
NOW=$(date +"%m-%d-%Y %T")
OUTFILE="/tmp/statusmon.log"
MAILFILE="/tmp/statusmon.eml"
RECCHKRETVAL=0
RAIDCHKRETVAL=0
ENGINECHKRETVAL=0


echo $NOW > $OUTFILE
chmod 666 $OUTFILE
echo >> $OUTFILE
/opt/statusmon1/recchk.sh >> $OUTFILE
RECCHKRETVAL=$?
sleep 3 
/opt/statusmon1/raidsubsyschk.sh >> $OUTFILE 
RAIDCHKRETVAL=$?
sleep 3
/opt/statusmon1/enginecheck.sh >> $OUTFILE
ENGINECHKRETVAL=$?
sleep 3

echo '#----#' >> $OUTFILE


if [[ $RAIDCHKRETVAL == 0 && $RECCHKRETVAL == 0 && $ENGINECHKRETVAL == 0 ]]
then 
  exit 0
fi

echo $NOW > $MAILFILE
chmod 666 $MAILFILE
echo "An anomaly has been detected on this device, please check its status" >> $MAILFILE
echo >> $MAILFILE
echo "The following issues have been detected:" >> $MAILFILE

i=1
POSCOUNT=3
ITEMCOUNT=1
until [[ $i -eq 99 ]]
do
FILESTR=`sed -n ''$POSCOUNT'p' $OUTFILE | cut -d"=" -f 1 `
STATCAL=`sed -n ''$POSCOUNT'p' $OUTFILE  | cut -d"=" -f 2 `  
if [[ $FILESTR == '#----#'  || $POSCOUNT -eq 20 ]]
then 
  break
fi

if [ $STATCAL -eq 0 ]
then
  nothing=1
else
  echo >> $MAILFILE
  echo -e  "$ITEMCOUNT)\c" >> $MAILFILE
  ITEMCOUNT=`expr $ITEMCOUNT + 1`
  case "$FILESTR" in
  DISKINCSTAT)
    echo "STATUS=$STATCAL: If 1: disk usage does not appear to be incrementing - please SSH on and issue a 'df -k' twice 30 secs apart.  /data should be incrementing. If -1: cleanupe is taking too long." >> $MAILFILE;;
  DISKCONT)
    echo  "Disk count does not appear to be contiguous - please SSH on and issue a 'cli32 disk info' to verify." >> $MAILFILE;;
  ENGINESTAT)
    echo  "Too many dengines appear to  be running simulataneously.  This could be normal if many users are decoding, however if you receive this alert multiple times the decode engines may be in a hung state.  In this case they may need to be terminated manually.
 To check please log onto the device and navigate to /opt/NetScout/rtm/bin then run ./PS. " >> $MAILFILE;;
  DISKSTATE)
    echo "Disk failure(s) detected, $STATCAL disk(s) appear to be in abnormal state - please SSH on and issue a 'cli32 disk info' to verify." >> $MAILFILE;;
  VOLSTATE)
     echo "The RAID volume appears to be abnormal - please SSH on and issue a 'cli32 rsf info' and 'cli32 vsf info' to verify" >> $MAILFILE;;
  esac
fi
POSCOUNT=`expr $POSCOUNT + 1`
i=`expr $i + 1`
done
if [ $MAILMODE -eq 1 ]                                                                                            
then                                                                                                              
        /opt/statusmon1/recalert.plx > /tmp/recalert.log                                                           
fi                                                                                                                                
