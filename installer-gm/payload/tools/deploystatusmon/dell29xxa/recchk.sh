#!/bin/bash -x
#recchk.sh
#
#
# VERSION 1.01 Steve Murphy 10-10-2012
#
#
DISKDIFF=0
INCCOUNT=0
DISKINCSTAT=0
SAFETY=0
SLEEPTIME=60
CLEANSLEEP=120
KEYW=/data

while [ $INCCOUNT -le 5 ]
do
        #Get disk space for /data/tfa
	DISKSP1=$(df -k | grep -i $KEYW | awk ' {print $3} ')
	sleep $SLEEPTIME
	DISKSP2=$(df -k | grep -i $KEYW | awk ' {print $3} ')
	DISKDIFF=`expr $DISKSP2 - $DISKSP1`	
	if [ $DISKDIFF -gt 0 ]
        then
		# If incrementing then set DISKINCSTAT 0 = ALL IS WELL
                DISKINCSTAT=0
                break
        elif [ $DISKDIFF -lt 0 ]
        then
		# If decrementing then sleep for cleanup sleep value and loop again to retest
                
		sleep $CLEANSLEEP
		DISKINCSTAT=-1
	else
		# If not incrementing OR decrementing then set DISKINCSTAT 1 = NO RECORDING
		DISKINCSTAT=1
	fi
	
       INCCOUNT=`expr $INCCOUNT + 1`
done

echo DISKINCSTAT=$DISKINCSTAT  

if [[ $DISKINCSTAT -eq 0 || $DISKINCSTAT -eq -1 ]]
then
#return value 0 if all is well
  exit 0
else
#return value 1 if something is wrong
  exit 1 
fi
