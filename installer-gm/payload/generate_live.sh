#!/bin/bash
PATH=/usr/kerberos/sbin:/usr/kerberos/bin:/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/opt/dell/srvadmin/bin:/opt/dell/srvadmin/sbin:/root/bin

cat /opt/nshealth/live_head.txt > /opt/nshealth/livestats.html

today=$(date +%Y%m%d)

cd /opt/nshealth

FILES=islive/*.txt
  for f in $FILES
  do

if grep -q  FAIL $f  &&  grep -q GOOD $f
then
  echo "<a href=\"$f\" class=\"tab Fgreen\">" >> /opt/nshealth/livestats.html
  echo "<span class=\"left\"></span>" >> /opt/nshealth/livestats.html
  echo "<span class=\"right\"></span>" >> /opt/nshealth/livestats.html
  echo "</a>" >> /opt/nshealth/livestats.html
elif grep -q FAIL $f
then
  echo "<a href=\"$f\" class=\"tab red\">" >> /opt/nshealth/livestats.html
  echo "<span class=\"left\"></span>" >> /opt/nshealth/livestats.html
  echo "<span class=\"right\"></span>" >> /opt/nshealth/livestats.html
  echo "</a>" >> /opt/nshealth/livestats.html
elif grep -q GOOD $f
then
  echo "<a href=\"$f\" class=\"tab green\">" >> /opt/nshealth/livestats.html
  echo "<span class=\"left\"></span>" >> /opt/nshealth/livestats.html
  echo "<span class=\"right\"></span>" >> /opt/nshealth/livestats.html
  echo "</a>" >> /opt/nshealth/livestats.html
else
echo "<a href=\"$f\" class=\"tab blue\">" >> /opt/nshealth/livestats.html
  echo "<span class=\"left\"></span>" >> /opt/nshealth/livestats.html
  echo "<span class=\"right\"></span>" >> /opt/nshealth/livestats.html
  echo "</a>" >> /opt/nshealth/livestats.html
fi
  done

cat live_middle.txt >> /opt/nshealth/livestats.html

cat /opt/nshealth/live_bottom.txt >> /opt/nshealth/livestats.html
