#!/bin/bash

dialog --title "Crontab Install" \
--backtitle "Health Check Installer v1.0" \
--infobox "  Added Dohealh in crontab for daily run" 4 50 ; sleep 2

crontab -l > date.cron

if ! grep -Fq "/opt/nshealth/consistency_check.sh" date.cron; then
echo '0 1 1 * * ./root/.bashrc; /opt/nshealth/consistency_check.sh >> /opt/nshealth/logs/consistency.log 2>&1' >> date.cron
fi

if ! grep -Fq "/opt/nshealth/dohealth" date.cron; then
echo '1 0 * * * ./root/.bashrc; /opt/nshealth/dohealth >> /opt/nshealth/logs/gmhealth.log 2>&1' >> date.cron
fi

if ! grep -Fq "/opt/nshealth/generate-gm.sh" date.cron; then
echo '15 6 * * * ./root/.bashrc; /opt/nshealth/generate-gm.sh >> /opt/nshealth/logs/generate.log 2>&1' >> date.cron
fi

if ! grep -Fq "/opt/nshealth/get_pm_status_files.sh" date.cron; then
echo '15 * * * * ./root/.bashrc; /opt/nshealth/get_pm_status_files.sh >> /tmp/pm_status.log 2>&1' >> date.cron
fi

if ! grep -Fq "/opt/nshealth/grabpmoutput" date.cron; then
echo '28 * * * * ./root/.bashrc; /opt/nshealth/grabpmoutput >> /opt/nshealth/logs/grabpmoutput.log 2>&1' >> date.cron
fi

crontab date.cron
rm -f date.cron

dialog --title "***Job added to Crontab***" \
--backtitle "Health Check Installer v1.0" \
--infobox "$(crontab -l)" 30 120 ; sleep 3

