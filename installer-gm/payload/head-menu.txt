<!doctype html>
<html lang="en" class="no-js">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>NetScout Systems</title>
  <meta name="description" content="NShealth PM buttons">
  <meta name="author" content="NetScout Systems">
  <link rel="stylesheet" href="css/demo.css">
  <link rel="stylesheet" href="css/inputs.css">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/coin-slider.css" />
<link rel="stylesheet" type="text/css" href="css/epoch_styles.css" /> <!--Epoch's styles-->
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/epoch_classes.js"></script> <!--Epoch's Code-->
<script type="text/javascript" src="js/cufon-yui.js"></script>
<script type="text/javascript" src="js/cufon-aller-700.js"></script>
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript" src="js/coin-slider.min.js"></script>
<script type="text/javascript">
  <!--[if lte IE 8]>
    <link rel="stylesheet" href="css/ie.css">
  <![endif]-->
  <!--[if lte IE 6]>
    <link rel="stylesheet" href="css/ie6.css">
  <![endif]-->

  <script src="js/modernizr-1.7.min.js"></script>
  <script>
    document.write('<style id="hide-css" type="text/css">.js-hide{display:none !important}</style>');
  </script>
</head>
<body>
  <div class="row" id="main" role="main">
        <div class="logo2">
        <h1> <a TARGET="content" href="index-gm.html">  NS<span>Health</span></a> <small><a>   Netscout Health Platform</small></a></h1>
      </div>
                <div class="column grid_10">
        <div class="row" id="examples">
        <div class="column grid_10">
          <div class="example">

