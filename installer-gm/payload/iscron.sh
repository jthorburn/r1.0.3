#!/bin/bash

crontab -l > date.cron

if ! grep -Fq "/root/ishealth/IScheck.sh" date.cron
then
touch date.cron
echo '*/10 * * * * ./root/.bashrc;/root/ishealth/write-chk.sh  > infinistreamhealth.log 2>&1' >> date.cron
fi
crontab date.cron
rm -f date.cron

