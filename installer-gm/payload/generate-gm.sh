#!/bin/bash

PATH=/usr/kerberos/sbin:/usr/kerberos/bin:/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/opt/dell/srvadmin/bin:/opt/dell/srvadmin/sbin:/root/bin

cd /opt/nshealth

cat /opt/nshealth/head-gm.txt > index-gm.html

ipaddress=$(ip addr show | grep inet | awk -F " " '{print $NF"|"$2}' | grep eth0 | cut -c 6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22 | cut -d/ -f1)
   echo "   <div class="logo">" >> /opt/nshealth/index-gm.html
   echo "   <h1> <small>Global Manager</small><small><span>$ipaddress</span></small> </a></h1>" >> /opt/nshealth/index-gm.html
   echo "   </div>" >> /opt/nshealth/index-gm.html


cat /opt/nshealth/head-gm2.txt >> index-gm.html


today=$(date +%Y%m%d)
FILES=pmhealth/*_$today.txt
  for i in $FILES
  do
 hostname=$(echo $i | cut -d/ -f2 | cut -d_ -f1)
 filename=$(echo $i | cut -d_ -f1)
echo "<option value="$filename\_gmhealth_AAAAMM.txt"> $hostname </option>"  >> /opt/nshealth/index-gm.html
done

cat /opt/nshealth/middle-gm1.txt >> index-gm.html

today=$(date +%Y%m%d)
FILES=pmhealth/*_$today.scrub
  for i in $FILES
  do
 hostname=$(echo $i | cut -d/ -f2 | cut -d_ -f1)
 filename=$(echo $i | cut -d_ -f1)
echo "<option value="$filename\_AAAAMM.scrub"> $hostname </option>"  >> /opt/nshealth/index-gm.html
done

cat /opt/nshealth/middle-gm2.txt >> /opt/nshealth/index-gm.html

FILES=pmlive/*_status.txt
  for f in $FILES
  do

if grep -q Complete $f
  then

ipaddr=$(echo $f | cut -d/ -f2 | cut -d_ -f1)

if egrep  -i "fail|Warn|Error|Bad|degraded|critical|missing|not|100%|99%" $f
then
echo "<a href=\"$f\" target=\"_blank\" class=\"btn btn-danger\" btn-small style=\"margin-left:auto;\"" >> /opt/nshealth/index-gm.html
  echo "</i>$ipaddr</a>" >> /opt/nshealth/index-gm.html
elif
grep Utilization $f | cut -d= -f4 | cut -d"," -f1 | egrep "95|96|97|98"
then
echo "<a href=\"$f\" target=\"_blank\" class=\"btn btn-warning\" btn-small style=\"margin-left:auto;\"" >> /opt/nshealth/index-gm.html
  echo "</i>$ipaddr</a>" >> /opt/nshealth/index-gm.html
else
echo "<a href=\"$f\" target=\"_blank\" class=\"btn btn-success\" btn-small style=\"margin-left:auto;\"" >> /opt/nshealth/index-gm.html
  echo "</i>$ipaddr</a>" >> /opt/nshealth/index-gm.html
fi
else
echo "<a href=\"$f\" target=\"_blank\" class=\"btn btn-primary\" btn-small style=\"margin-left:auto;\"" >> /opt/nshealth/index-gm.html
  echo "</i>$ipaddr</a>" >> /opt/nshealth/index-gm.html
fi
  done

cat /opt/nshealth/bottom-gm.txt >> /opt/nshealth/index-gm.html
