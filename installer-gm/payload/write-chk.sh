#!/bin/bash

/bin/mv -f /tmp/status.txt /tmp/last_status.txt 2> /dev/null

my_ip=$(/sbin/ifconfig eth0 | grep -w inet |awk {'print $2'}|cut -d":" -f2)
#starttime=$(date --date="5 minutes ago" +%Y-%m-%d/%H:%M)
#endtime=$(date +%Y-%m-%d/%H:%M)

starttime=$(date --date="5 minutes ago" +%H:%M:00)
endtime=$(date +%H:%M:00)
#echo $starttime
#echo $endtime


/bin/date > /tmp/status.txt
echo "" >> /tmp/status.txt
/bin/hostname >> /tmp/status.txt
echo "" >> /tmp/status.txt
echo "Checking if data has been written to any interface in last 5 minutes."  >> /tmp/status.txt
echo ""  >> /tmp/status.txt

# Grab the interface values from the .flowconfig file. Grep using IP since I have seen older IP's appear in the file. This seems to be best generic way to get the interface numbers.
# Once we have the interface numbers run the appIdxUtil command to verify if 5 packets have been written in last 5 minutesi to each interface. The 5 packets is just to limit processing of the store.
# For each interface write a good or fail status to the /tmp/status file.

#/opt/NetScout/rtm/tools/read_dotflowcfg /opt/NetScout/rtm/bin/.flowconfig | grep $my_ip | cut -d= -f3 | awk '{print $1}'| while read ifval

#ls -1rt /opt/NetScout/rtm/log/log_* | tail -1 | while read a ; do grep CDMFLOWS $a | grep activated | awk '{print $6}

latest_log=$(ls -1rt /opt/NetScout/rtm/log/log_* | tail -1)

if /bin/grep CDMFLOWS $latest_log | /bin/grep -q activated > /tmp/temp_int_log
then
/bin/grep CDMFLOWS $latest_log | /bin/grep -v deactivated | awk '{print $6}' | sort | uniq | while read ifval
 do
  /opt/NetScout/rtm/tools/appIdxUtil /data/tfa/"$my_ip""_if$ifval" $starttime $endtime -l 5 | if grep -q "frames processed"
    then echo "$my_ip""_if$ifval"" = GOOD" >>/tmp/status.txt
      else echo "$my_ip""_if$ifval"" = FAIL" >> /tmp/status.txt
fi
done
else
echo "CDMFLOWS NOT ACTIVATED" >>/tmp/status.txt
fi

#cleanup any old files from running the appIdxUtil tool
rm -rf /root/core.appIdxUtil.*
rm -rf /root/appIdxUtil_*



