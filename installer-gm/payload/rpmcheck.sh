#!/bin/bash

rpm -qa | if grep ^dialog
then
echo "RPM file already exists, starting Health Check installer"
sleep 1
else
echo "Installing RPM to run installer"
rpm -ivh dialog-1.1-9.20080819.1.el6.x86_64.rpm
echo
echo "RPM installation completed. Please run the installer again."
fi
