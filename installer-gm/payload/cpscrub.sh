#!/bin/bash
#cpscrub.sh

#Copy Scrub files over to /opt/nshealth/ishealth and /opt/nshealth/pmhealth

dialog --title "Scrub Files Install" \
--backtitle "Health Check Installer v1.0" \
--infobox "Installing scrub files" 4 40 ; sleep 2


if [ ! -d "/opt/nshealth/ishealth" ]; then
echo
dialog --title "Scrub Files Install" \
--backtitle "Health Check Installer v1.0" \
--infobox "There is no 'ishealth' directory" 4 40 ; sleep 2
else

cp scr* /opt/nshealth/ishealth

# Change permissions on scrub script files

chmod +x /opt/nshealth/ishealth/*

dialog --title "Scrub Files Install" \
--backtitle "Health Check Installer v1.0" \
--infobox "scrub files has been copied to 'ishealth' directory" 4 55 ; sleep 2

dialog --title "Scrub Files Installed" \
--backtitle "Health Check Installer v1.0" \
--infobox "$(du --time /opt/nshealth/ishealth/sc* | cut -f 2,3)" 6 65 ; sleep 2
fi

if [ ! -d "/opt/nshealth/pmhealth" ]; then
echo
dialog --title "Scrub Files Install" \
--backtitle "Health Check Installer v1.0" \
--infobox "There is no 'pmhealth' directory" 4 40 ; sleep 2
else

cp scr* /opt/nshealth/pmhealth

# Change permissions on scrub script files

chmod +x /opt/nshealth/pmhealth/*

rm -f /opt/scr* &

dialog --title "Scrub Files Install" \
--backtitle "Health Check Installer v1.0" \
--infobox "scrub files has been copied to 'pmhealth' directory" 4 55 ; sleep 2

dialog --title "Scrub Files Installed" \
--backtitle "Health Check Installer v1.0" \
--infobox "$(du --time /opt/nshealth/pmhealth/sc* | cut -f 2,3)" 6 65 ; sleep 2
fi
