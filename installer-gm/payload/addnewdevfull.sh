#!/bin/bash

1=""
2=""
3=""
4=""
5=""
6=""
7=""
8=""
9=""
10=""
 
# open fd
exec 3>&1
 
# Store data to $VALUES variable
VALUES=$(dialog --ok-label "Submit" \
	  --backtitle "HealthCheck installer v1.0" \
	  --title "New PM(s) to be added in HealthCheck " \
	  --form "Add new IPs" \
15 50 0 \
	"IP Address 1:"     1 1 "$1" 	1 15 15 0 \
	"IP Address 2:"     2 1	"$2"  	2 15 15 0 \
	"IP Address 3:"     3 1	"$3"  	3 15 15 0 \
	"IP Address 4:"     4 1	"$4" 	4 15 15 0 \
	"IP Address 5:"     5 1 "$5"    5 15 15 0 \
        "IP Address 6:"     6 1 "$6"    6 15 15 0 \
        "IP Address 7:"     7 1 "$7"    7 15 15 0 \
        "IP Address 8:"     8 1 "$8"    8 15 15 0 \
	"IP Address 9:"     9 1 "$7"    9 15 15 0 \
        "IP Address 10:"    10 1 "$8"   10 15 15 0 \
2>&1 1>&3)
 
# close fd
exec 3>&-
 
# display values just entered
echo "$VALUES" > /opt/nshealth/pmip.csv

