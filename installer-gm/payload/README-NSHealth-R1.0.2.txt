NSHealth Readme File - Version 1.0.2

Fixes included in this patch:
1) Script debug options for "generate.sh" and "consistency_check.sh" were removed
2) Scripts being called from "get_is_status_files.sh" will now work properly
3) Added check for corefiles in /opt/NetScout/rtm/bin on Infinistreams
4) Script "crontab.sh" had incorrect syntax for accessing root user's .bashrc file (ensures PATH for applications used in the script are correct)
5) Removed customer specific label from all relevant files
6) Superodoctor command was running twice on WB1 - it will now only run once
7) Identification of WB 0 devices was incorrect causing tw_cli commands to try to run on WB 1 devices. WB 0 devices are now correctly identified and tw_cli commands will not run against WB 1 devices.
8) GM installer Options 5 and 6 were referencing Infinistream IP files vs. PM IP list files
9) GM installer file removed the option to "Create RSA keys to PM's". A new Option 7 was added that will add new/additional PM's and generate the RSA keys to these devices
10) A maximum of 10 PM's could be added when installing the GM NSHealth. Now the user will be prompted if more PM's will added when the Submit entry is chosen.


Known Issues:
1) When installing or patching the NSHealth application, users will need to back up the data files created by prior installations. Copy the following directories to the /opt partition :
	cp -r /opt/nshealth/ishealth /opt/
	cp -r /opt/nshealth/islive /opt/
	cp -r /opt/nshealth/pmhealth /opt/
	cp -r /opt/nshealth/pmlive /opt/

* NOTE- GM server installations will only have "pmhealth" and "pmlive" directories

Once the patch installation is complete, restore these directories to the /opt/nshealth directory :

	cp -r /opt/ishealth /opt/nshealth/
	cp -r /opt/ishealth /opt/nshealth/
	cp -r /opt/pmhealth /opt/nshealth
	cp -r /opt/pmlive /opt/nshealth

2) Key generation will continue to run for PM's and Infinistreams where either the password is incorrectly entered or the device is not reachable via SSH

3) When installing the GM NSHealth application, use of the number pad keys when adding IP addresses for PM servers will cause the installation to continue without adding any servers. You MUST use the number keys on the top of the keyboard to add these IP addresses.

4) When adding PM devices after the insitial installation you must copy and install the "nshealth-R1.0.2.bin" to the PM server in order for checks to complete correctly.
